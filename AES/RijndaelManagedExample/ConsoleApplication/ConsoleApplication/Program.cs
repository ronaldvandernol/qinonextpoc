﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            string stringToEncrypt = "This string should be encrypted the same from .net and javascript.";

            SalesDashboard.Encryption.Encryption aes = new SalesDashboard.Encryption.Encryption("52855327-D1DC-4D");
            string encrypted = aes.Encrypt(stringToEncrypt);
            Console.WriteLine($"encrypttest: {encrypted}");
            string decrypted = aes.Decrypt(encrypted);
            Console.WriteLine($"decrypttest: {decrypted}");
            Console.Read();
        }
    }
}
