﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AESConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Give the username: ");
            string username = Console.ReadLine();

            string jsonString = @"{ ""username"":""" + username + @""", ""datetime"":""" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + @"""}";
            Encryption aesEncrypt = new Encryption();
            string encrypted = aesEncrypt.OpenSSLEncrypt(jsonString, "a2e94370432f9ed0e2bc56be74c9120008eb7f30086df442ad5c87ce40da0ad3");
            Console.WriteLine("http://localhost:54109/saas/index.asp?aeskey=" + encrypted);
            
            Console.Read();
        }
    }
}