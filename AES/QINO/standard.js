function fnResizeMainWindow()
{
    var height = parseInt(maintemp.clientHeight);

    if (navigator.userAgent.indexOf('iPhone') > 0)
    {
        main.style.height = (height + 120) + "px";
        maintop.style.height = (height + topbarsize) + "px";
    }
    else
    {
        main.style.height = height + "px";
        maintop.style.height = (height - topbarsize) + "px";
    }
    mainbot.style.height = topbarsize + "px";
}

function fnOpenFrame(x, y, w, h, id, bGrow, bCloseBox, bSize, bRefresh, title, fnComplete, fnCloseProc, fnHelpFunc, classname)
{
    var oFrame = document.createElement("DIV");
    var stepy = h / 40;
    var stepx = w / 40;

    oFrame.id = id;

    if (classname != '')
        oFrame.className = classname;
    else
        oFrame.className = "brainumwindow";

    oFrame.style.top = (y + h / 2) + "px";
    oFrame.style.left = (x + w / 2) + "px";
    oFrame.style.height = "1px";
    oFrame.style.width = "1px";
    maintop.insertAdjacentElement("beforeEnd", oFrame);

    if (bGrow)
        setTimeout("fnGrowFrame(1, " + (x + w / 2) + ", " + (y + h / 2) + ", " + stepx + ", " + stepy + ", " + id + ", " + bCloseBox + ", " + bRefresh + ", " + bSize + ", '" + fnComplete + "', '" + title + "', " + fnCloseProc + ", " + fnHelpFunc + ")", 2);
    else
        fnGrowFrame(20, (x + w / 2), (y + h / 2), stepx, stepy, document.getElementById(id), bCloseBox, bRefresh, bSize, fnComplete, title, fnCloseProc, fnHelpFunc);
}

function fnGrowFrame(step, x, y, stepx, stepy, obj, bCloseBox, bRefresh, bSize, fnComplete, title, fnCloseProc, fnHelpFunc)
{
    obj.style.top = (y - stepy * step) + "px";
    obj.style.left = (x - stepx * step) + "px";
    obj.style.height = (stepy * step * 2) + "px";
    obj.style.width = (stepx * step * 2) + "px";

    if (step < 20)
        setTimeout("fnGrowFrame(" + (step + 1) + ", " + x + ", " + y + ", " + stepx + ", " + stepy + ", " + obj.id + ", " + bCloseBox + ", " + bRefresh + ", " + bSize + ", '" + fnComplete + "', '" + title + "', " + fnCloseProc + ", " + fnHelpFunc + ")", 50);
    else
    {
        if (fnComplete != '')
            eval(fnComplete);

        if (typeof title != 'undefined' && title.length > 0)
        {
            var x = (parseInt(obj.style.width) - 23);
            var oTitle = document.createElement("DIV");
            obj.insertAdjacentElement("beforeEnd", oTitle);

            oTitle.id = obj.id + "title";
            oTitle.className = "brainumwindowtitle";
            oTitle.style.height = "18px";
            oTitle.style.width = (parseInt(obj.style.width) - 8) + "px";
            oTitle.style.top = "1px";
            oTitle.style.left = "1px";
            oTitle.innerHTML = "<span>" + title + "</span>";

            if (bCloseBox)
            {
                var oCloseBox = document.createElement("DIV");
                oCloseBox.className = "windowbox";
                oCloseBox.innerHTML = "<img style='width: 18px; width: 18px; cursor: pointer;' src='/system/client/images/iconset/cross.png' />";

                oCloseBox.title = szCloseScreen;
                oCloseBox.style.top = "1px";
                oCloseBox.style.left = x + "px";

                if (fnCloseProc)
                    oCloseBox.onclick = fnCloseProc;
                else
                    oCloseBox.onclick = fnCloseWindow;

                x -= 20;

                oCloseBox.onmouseover = fnHandleCloseBox;
                oCloseBox.onmouseout = fnHandleCloseBox;
                oCloseBox.onmousemove = fnHandleCloseBox;
                oCloseBox.onmousedown = fnHandleCloseBox;
                oCloseBox.oncontextmenu = fnHandleCloseBox;
                oCloseBox.onmouseup = fnHandleCloseBox;

                oTitle.onmouseover = fnHandleMove;
                oTitle.onmouseout = fnHandleMove;
                oTitle.onmousemove = fnHandleMove;
                oTitle.onmousedown = fnHandleMove;
                oTitle.oncontextmenu = fnHandleMove;
                oTitle.onmouseup = fnHandleMove;
                oTitle.insertAdjacentElement("beforeEnd", oCloseBox);
            }
           
            if (bRefresh)
            {
                var oRefreshBox = document.createElement("DIV");

                oRefreshBox.innerHTML = "<img style='width: 18px; width: 18px; cursor: pointer;' src='/system/client/images/iconset/arrow_rotate_anticlockwise.png' />";
                oRefreshBox.title = szVerversen;
                oRefreshBox.className = "windowbox";
                oRefreshBox.style.top = "1px";
                oRefreshBox.style.left = x + "px";
                oRefreshBox.onclick = fnRefreshWindow;

                x -= 20;

                oRefreshBox.onmouseover = fnHandleRefreshBox;
                oRefreshBox.onmouseout = fnHandleRefreshBox;
                oRefreshBox.onmousemove = fnHandleRefreshBox;
                oRefreshBox.onmousedown = fnHandleRefreshBox;
                oRefreshBox.oncontextmenu = fnHandleRefreshBox;
                oRefreshBox.onmouseup = fnHandleRefreshBox;

                oTitle.insertAdjacentElement("beforeEnd", oRefreshBox);
            }

            if (fnHelpFunc)
            {
                var oHelpBox = document.createElement("DIV");

                oHelpBox.innerHTML = "<img src='images/icons16/help.png' />";
                oHelpBox.title = szHelp;
                oHelpBox.className = "windowbox";
                oHelpBox.style.top = "1px";
                oHelpBox.style.left = x + "px";
                oHelpBox.onclick = fnHelpFunc;

                x -= 20;

                oHelpBox.onmouseover = fnHandleHelpBox;
                oHelpBox.onmouseout = fnHandleHelpBox;
                oHelpBox.onmousemove = fnHandleHelpBox;
                oHelpBox.onmousedown = fnHandleHelpBox;
                oHelpBox.oncontextmenu = fnHandleHelpBox;
                oHelpBox.onmouseup = fnHandleHelpBox;

                oTitle.insertAdjacentElement("beforeEnd", oHelpBox);
            }
        }
    }
}

function fnMaxWindow()
{
    var target = this;
    var obj = target.parentElement.parentElement;
    var wdno = -1;

    for (var i = 0; i < iMDIWindowsOpen; i++)
    {
        if (obj.id == iMDIOpenWindowsArray[i])
        {
            wdno = i;
            break;
        }
    }

    if (wdno >= 0)
    {
        obj.style.top = "0px";
        obj.style.left = "0px";
        obj.style.height = (maintop.clientHeight + 60) + "px";
        obj.style.width = (maintop.clientWidth - 0) + "px";

        var titleobj = obj.children[1];
        var x = (parseInt(obj.style.width) - 22);
        titleobj.style.width = (parseInt(obj.style.width) - 10) + "px";

        obj.children[0].style.width = (obj.clientWidth - 2) + "px";
        obj.children[0].style.height = (obj.clientHeight - (bMDIOpenWindowsArraySize[wdno] ? 32 : 26)) + "px";
        obj.children[2].style.left = (obj.clientWidth - 8) + "px";
        obj.children[2].style.top = (obj.clientHeight - 8) + "px";

        for (var i = 1; i < titleobj.children.length; i++)
        {
            titleobj.children[i].style.left = x + "px";
            x -= 17;

            if (szMDIOpenWindowsArrayURL[wdno].indexOf("screen.asp") > 0)
                fnRefreshWindowGo(wdno);
        }
    }
    this.cancelBubble = true;
}

function fnMinWindow()
{
    var target = this;
    target.parentElement.parentElement.style.display = 'none';
    this.cancelBubble = true;
}

function fnRefreshWindow()
{
    var target = this;
    var obj = target.parentElement.parentElement;

    for (var i = 0; i < iMDIWindowsOpen; i++)
    {
        if (obj.id == iMDIOpenWindowsArray[i])
        {
            fnRefreshWindowGo(i);
            break;
        }
    }
}

function fnRefreshWindowGo(wdno)
{
    if (wdno >= 0)
    {
        try
        {
            var obj = document.getElementById(iMDIOpenWindowsArray[wdno]);
            var url = szMDIOpenWindowsArrayURL[wdno];
            var p = url.indexOf("&scrwidth=");

            if (p > 0)
            {
                url = url.substr(0, p);
                url += "&scrwidth=" + (obj.clientWidth - 2);
                url += "&scrheight=" + (obj.clientHeight - (bMDIOpenWindowsArraySize[wdno] ? 32 : 26));
                url += "&nocache=" + (new Date().getTime()) + "&refresh=" + obj.children[0].contentWindow.document.body.children[1].getAttribute("_screenindex");

                SrvCommNaviDelete(obj.children[0].contentWindow.document.body.children[1].getAttribute("_screenindex"));
            }

            var oObject = document.createElement("iframe");

            try 
            { 
                obj.removeChild(obj.children[0]); 
            } 
            catch (e) { }

            oObject.onreadystatechange = fnCheckComplete;
            fnOpenWaitWindow();

            oObject.src = url;
            oObject.frameBorder = "no";
            oObject.style.position = "absolute";
            oObject.style.top = "24px";
            oObject.style.left = "0px";
            oObject.style.width = (obj.clientWidth - 2) + "px";
            oObject.style.height = (obj.clientHeight - (bMDIOpenWindowsArraySize[wdno] ? 32 : 26)) + "px";
            obj.insertAdjacentElement("afterBegin", oObject);

            fnMDIMouseDown(obj);
            setTimeout('fnCloseWaitWindow();', 2000);
        } 
        catch (e) 
        { 
            alert(e); 
        }
    }
}

var bWindowMouseDown = false;
var iWindowStartY;
var iWindowStartX;
var oMoveElement;
var oCurrentElement = null;

function fnHandleResizeBox()
{
    var oElement;

    if (oCurrentElement)
    {
        oElement = oCurrentElement;
    }
    else
    {
        var target = this;
        oElement = target.parentElement;

        if (oElement.parentElement.id == "")
            return;
    }

    if (event.type == "mousedown" && event.button == 0)
    {
        oElement.style.cursor = "se-resize";
        oCurrentElement = oElement;
        oMoveElement = oElement.parentElement;
        bWindowMouseDown = true;
        iWindowStartY = event.screenY;
        iWindowStartX = event.screenX;
        fnMDIMouseDown(oMoveElement);
    }
    else
    {
        if (event.type == "mouseup" && bWindowMouseDown)
        {
            oCurrentElement.style.cursor = "";

            var obj = oMoveElement.children[1];
            var x = (parseInt(oMoveElement.style.width) - 22);
            obj.style.width = (parseInt(oMoveElement.style.width) - 10) + "px";

            for (var i = 1; i < obj.children.length; i++)
            {
                obj.children[i].style.left = x + "px";
                x -= 17;
            }

            var wdno = -1;
            for (var i = 0; i < iMDIWindowsOpen; i++)
            {
                if (oMoveElement.id == iMDIOpenWindowsArray[i])
                {
                    wdno = i;
                    break;
                }
            }

            if (wdno >= 0 && szMDIOpenWindowsArrayURL[wdno].indexOf("screen.asp") > 0)
                fnRefreshWindow(wdno);

            bWindowMouseDown = false;
            oCurrentElement = null;
        }
        else if (event.type == "mousemove" && bWindowMouseDown)
        {
            if ((parseInt(oMoveElement.style.height) + event.screenY - iWindowStartY) >= 100 && (parseInt(oMoveElement.style.width) + event.screenX - iWindowStartX) >= 100)
            {
                oCurrentElement.style.top = (parseInt(oCurrentElement.style.top) + event.screenY - iWindowStartY) + "px";
                oCurrentElement.style.left = (parseInt(oCurrentElement.style.left) + event.screenX - iWindowStartX) + "px";
                oMoveElement.style.height = (parseInt(oMoveElement.style.height) + event.screenY - iWindowStartY) + "px";
                oMoveElement.style.width = (parseInt(oMoveElement.style.width) + event.screenX - iWindowStartX) + "px";
                oMoveElement.children[0].style.height = (parseInt(oMoveElement.children[0].style.height) + event.screenY - iWindowStartY) + "px";
                oMoveElement.children[0].style.width = (parseInt(oMoveElement.children[0].style.width) + event.screenX - iWindowStartX) + "px";
                iWindowStartY = event.screenY;
                iWindowStartX = event.screenX;
            }
        }        
    }
}

function fnHandleHelpBox(thisEvent)
{
    if (thisEvent.type == "mouseout")
        thisEvent.target.src = "images/icons16/help.png";
    else if (thisEvent.type == "mouseover")
        thisEvent.target.src = "images/icons16/help-neon.png";
}

function fnHandleRefreshBox(thisEvent) {}

function fnHandleMinBox(thisEvent)
{
    if (thisEvent.type == "mouseout")
        thisEvent.target.src = "images/icons16/min.png";
    else if (thisEvent.type == "mouseover")
        thisEvent.target.src = "images/icons16/min-neon.png";
}

function fnHandleMaxBox(thisEvent)
{
    if (thisEvent.type == "mouseout")
        thisEvent.target.src = "images/icons16/max.png";
    else if (thisEvent.type == "mouseover")
        thisEvent.target.src = "images/icons16/max-neon.png";
}

function fnHandleCloseBox(thisEvent) {}

function fnHandleMove(thisEvent)
{
    var oElement = thisEvent.target;

    if (oElement.id == "")
        return;

    if (thisEvent.type == "mousedown" && thisEvent.button == 1)
    {
        oElement.style.cursor = "move";
        oElement.setCapture();
        oMoveElement = oElement.parentElement;
        bWindowMouseDown = true;
        iWindowStartY = thisEvent.clientY;
        iWindowStartX = thisEvent.clientX;
        fnMDIMouseDown(oMoveElement);
    }
    else if (thisEvent.type == "mouseup" && bWindowMouseDown)
    {
        oElement.style.cursor = "";
        oElement.releaseCapture();
        bWindowMouseDown = false;
    }
    else if (thisEvent.type == "mousemove" && bWindowMouseDown)
    {
        oMoveElement.style.top = (parseInt(oMoveElement.style.top) + thisEvent.clientY - iWindowStartY) + "px";
        oMoveElement.style.left = (parseInt(oMoveElement.style.left) + thisEvent.clientX - iWindowStartX) + "px";
        iWindowStartY = thisEvent.clientY;
        iWindowStartX = thisEvent.clientX;
    }        
}

function fnLogin(CompanyList, CurrentCompanyId)
{
    //fnOpenFrame((maintop.clientWidth - 600) / 2, topbarsize + (maintop.clientHeight - 350) / 2, 600, 310, 'loginwindow', false, false, false, false, szLogonTitle, 'fnLoginWindowReady("' + CompanyList + '","' + CurrentCompanyId + '");', null, null, '');
    fnOpenFrame(1, 1, 1, 1, 'loginwindow', false, false, false, false, szLogonTitle, 'fnLoginWindowReady("' + CompanyList + '","' + CurrentCompanyId + '");', null, null, '');
}

function fnLoginWindowReady(CompanyList, CurrentCompanyId)
{
    var lu = new Cookie();
    var szStoredName = lu.Get("Brainum User");

    fnCreateLabel(loginwindow, 'lbllogonname', 50, 70, 90, 20, szInlognaam);
    fnCreateInput(loginwindow, 'logonname', 135, 68, 220, 19, 'text', true);
    fnCreateLabel(loginwindow, 'lblpassword', 50, 98, 90, 20, szWachtwoord);
    fnCreateInput(loginwindow, 'password', 135, 96, 220, 19, 'password', false);

    if ((CompanyList.indexOf(";") >= 0) &&                      // Empty CompanyList
        (CompanyList.length > CompanyList.indexOf(";") + 1))    // Only one Company
    {
        fnCreateLabel(loginwindow, 'lblcompany', 50, 126, 90, 20, szLogonCompany);
        fnCreateLogonCompany(loginwindow, 'company', 135, 124, 220, 19, CompanyList, CurrentCompanyId);
    }

    fnCreateCheckbox(loginwindow, 'tabletmode', 400, 90, 200, 20, szTabletMode);
    fnCreateCheckbox(loginwindow, 'savename', 400, 70, 200, 20, szInlognaamonthouden);
    fnCreateButton(loginwindow, 'btlogin', 259, 152, 100, 25, szInloggen, fnOnLogon);
    fnCreateLabel(loginwindow, 'lblbrainum', 50, 180, 250, 80, "<img id=brainumlogo title='Brainum' onclick=\"location = 'http://www.brainum.nl';\" style=\"width: 250px; height: 79px; \" src='/images/BrainumLogo.png' />");

    password.onkeyup = function() { if (event.keyCode == 13) fnOnLogon(); }

    if (szStoredName)
    {
        logonname.value = szStoredName;
        savename.checked = true;
        document.getElementById("password").focus();

        if (lu.Get("Brainum tabletmode") == "true")
            tabletmode.checked = true;
    }
    else
    {
        document.getElementById("logonname").focus();
    }
}

function setlan(lan)
{
    var lu = new Cookie();
    var year = new Date();
    year.setFullYear(3000);
    lu.Set("brainumlang", lan, year);
    document.location = "index.asp?setLang=" + lan + "&ok=1";
}

var bLogonResponseFound = false;
function fnOnLogon()
{
    var lu = new Cookie();

    if (logonname.value == '')
    {
        fnMsgBox(szFoutmelding, szUmoeteeninlognaamingeven);
        logonname.focus();
        return;
    }

    fnOpenWaitWindow();

    if (savename.checked)
    {
        lu.Set("Brainum User", logonname.value);
        lu.Set("Brainum tabletmode", (tabletmode.checked) ? 'true' : 'false')
    }
    else
    {
        lu.Delete("Brainum User");
    }

    btlogin.disabled = true;
    bLogonResponseFound = false;
    setTimeout('fnNoLogon();', 10000);
    setTimeout('SrvCommLogin();', 10);
}

function SrvCommLogin(name, pw)
{
    var SelectedCompany = (typeof company == "undefined" ? null : company.value);
    var check = xmlhttp();
    var bi = new browserInfo();

    check.open("post", "/system/server/user/checkuseraes.asp", false);
    check.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    var ie = (bi.name == "Internet Explorer") ? 1 : 0;
    check.send("username=" + encodeURIComponent(name) + "&tablet=" + ((tabletmode.checked) ? '1' : '0') + "&ie=" + ie + "&compid=" + SelectedCompany + "&saas=1&nocache=" + (new Date().getTime()));

    if (check.status == 200)
    {
        try { eval(check.responseText); } catch (e) { }
        ShowTopMenu();
    }
    else
    {
        alert("Communication error: " + check.statusText);
    }
}

function fnNoLogon()
{
    if (!bLogonResponseFound)
    {
        fnCloseWaitWindow();
        fnMsgBox(szFoutmelding, szErisgeenantwoordvandeserverontvangenverversdepaginaenprobeeropnieuwaub);
        btlogin.disabled = false;
    }
}

function fnCloseWindow()
{
    fnCloseFrame(this.parentElement.parentElement, false);
}

function fnCloseFrame(obj, bShrink)
{
    var x = parseInt(obj.style.left);
    var y = parseInt(obj.style.top);
    var w = parseInt(obj.style.width);
    var h = parseInt(obj.style.height);
    var stepy = h / 40;
    var stepx = w / 40;

    try
    {
        while (obj.children.length)
            obj.removeChild(obj.children[0]);
    } 
    catch (e) { }

    if (bShrink)
        setTimeout("fnShrinkFrame(1, " + x + ", " + y + ", " + stepx + ", " + stepy + ", " + obj.id + ")", 2);
    else
        fnShrinkFrame(20, x, y, stepx, stepy, obj);
}

function fnShrinkFrame(step, x, y, stepx, stepy, obj)
{
    obj.style.top = (y + stepy * step) + "px";
    obj.style.left = (x + stepx * step) + "px";
    obj.style.height = (stepy * (20 - step) * 2) + "px";
    obj.style.width = (stepx * (20 - step) * 2) + "px";

    if (step < 20)
        setTimeout("fnShrinkFrame(" + (step + 1) + ", " + x + ", " + y + ", " + stepx + ", " + stepy + ", " + obj.id + ")", 2);
    else
        maintop.removeChild(obj);
}

function SrvCommAfterLogin()
{

    var check = xmlhttp();
    check.open("post", "/system/server/user/loginok.asp", false);
    check.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    check.send("&nocache=" + (new Date().getTime()));

    if (check.status == 200)
    {
        try 
        {
            eval(check.responseText);
        }
        catch (e) { }
    }
    else
    {
        alert("Communication error: " + check.statusText);
    }
}

function SrvCommGetScreenInfo(id, name)
{
    var check = xmlhttp();

    check.open("post", "/system/server/screen/getscreeninfo.asp", false);
    check.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    check.send("id=" + encodeURIComponent(id) + "&name=" + encodeURIComponent(name) + "&nocache=" + (new Date().getTime()));

    if (check.status == 200)
        return check.responseText;
    else
        alert("Communication error: " + check.statusText);

    return '';
}

var bLoggedOn = false;
function fnLogonResponse(szResult)
{
    try
    {
        bLogonResponseFound = true;
        fnCloseWaitWindow();
        if (szResult == "OK")
        {
            bLoggedOn = true;
            fnCloseFrame(loginwindow, false);
            SrvCommAfterLogin();
        }
        else if (szResult == "ERR")
        {
            btlogin.disabled = false;
            fnShowLoginError();
        }
        else if (szResult == "DBERR")
        {
            btlogin.disabled = false;
            fnMsgBox(szFoutmelding, szDeserverkandedatabasenietbenaderenprobeerhetlaternogeensaub + '. (' + szResult + ')');
        }
        else if (szResult == "EXPIRED")
        {
            btlogin.disabled = false;
            fnMsgBox(szFoutmelding, szUwsessieisverlopenverversdepaginaenprobeeropnieuwaub + '. (' + szResult + ')');
        }
        else
        {
            fnMsgBox(szFoutmelding, szEriseenongeldigvandeserverontvangenverversdepaginaenprobeeropnieuwaub + '. (' + szResult + ')');
        }
    } 
    catch (e) 
{ 
        alert(e) 
    }
}

function fnCreateInput(target, id, x, y, w, h, type, bMustInput)
{
    var oObject = document.createElement("input");

    oObject.id = id;
    oObject.type = type;

    if (bMustInput)
    {
        oObject.className = "editboxmustinput";
        oObject.onfocus = "this.className = 'editboxmustinputactive'";
        oObject.onblur = "this.className = 'editboxmustinput'";
    }
    else
    {
        oObject.className = "editbox";
        oObject.onfocus = "this.className = 'editboxactive'";
        oObject.onblur = "this.className = 'editbox'";
    }

    oObject.style.top = y + "px";
    oObject.style.left = x + "px";
    oObject.style.height = h + "px";
    oObject.style.width = w + "px";
    target.insertAdjacentElement("beforeEnd", oObject);
}

function fnCreateLabel(target, id, x, y, w, h, szLabel, szAlign)
{
    var oObject = document.createElement("DIV");

    oObject.id = id;
    oObject.className = "static";
    oObject.style.top = y + "px";
    oObject.style.left = x + "px";
    oObject.style.height = h + "px";
    oObject.style.width = w + "px";
    oObject.innerHTML = szLabel;

    if (typeof szAlign != 'undefined')
        oObject.style.textAlign = szAlign;

    target.insertAdjacentElement("beforeEnd", oObject);
}

function fnCreateButton(target, id, x, y, w, h, szLabel, fnOnClickFunc)
{
    var oObject = document.createElement("button");

    oObject.id = id;
    oObject.className = "button";
    oObject.style.top = y + "px";
    oObject.style.left = x + "px";
    oObject.style.height = h + "px";
    oObject.style.width = w + "px";
    oObject.onclick = fnOnClickFunc;
    oObject.innerText = szLabel;

    target.insertAdjacentElement("beforeEnd", oObject);
}

function fnCreateCheckbox(target, id, x, y, w, h, szLabel)
{
    var oObject = document.createElement("label");

    oObject.className = "static";
    oObject.style.top = y + "px";
    oObject.style.left = x + "px";
    oObject.style.height = h + "px";
    oObject.style.width = w + "px";

    var oCheckbox = document.createElement("input");

    oCheckbox.id = id;
    oCheckbox.type = "checkbox";
    oCheckbox.onfocus = "this.parentElement.className = 'staticactive'";
    oCheckbox.onblur = "this.parentElement.className = 'static'";
    oObject.insertAdjacentElement("beforeEnd", oCheckbox);

    var oLabel = document.createElement("span");

    oLabel.innerText = szLabel;
    oLabel.onfocus = "this.parentElement.className = 'staticactive'";
    oLabel.onblur = "this.parentElement.className = 'static'";
    oObject.insertAdjacentElement("beforeEnd", oLabel);

    target.insertAdjacentElement("beforeEnd", oObject);
}

function fnCreateLogonCompany(target, id, x, y, w, h, CompanyList, CurrentCompanyId)
{
    var oObject = document.createElement("select");

    oObject.id = id;
    oObject.className = "editbox";
    oObject.style.top = y + "px";
    oObject.style.left = x + "px";
    oObject.style.height = h + "px";
    oObject.style.width = w + "px";

    var LengthId = 38; // UniqueIdentifier = 38 positions
    var CompanyListEdit = CompanyList;
    var SeperationMarkerPosition = CompanyListEdit.indexOf(";");
    var NumberOfCompanies = 0;

    while ((SeperationMarkerPosition >= 0) && (NumberOfCompanies < 100))
    {
        var oOption = document.createElement("option");
        oOption.value = CompanyListEdit.substr(0, LengthId);
        oOption.text = CompanyListEdit.substr(LengthId, SeperationMarkerPosition - LengthId);

        if (oOption.value == CurrentCompanyId)
            oOption.selected = true;

        oObject.appendChild(oOption);

        CompanyListEdit = CompanyListEdit.substr(SeperationMarkerPosition + 1)
        SeperationMarkerPosition = CompanyListEdit.indexOf(";");
        NumberOfCompanies++;
    }

    target.insertAdjacentElement("beforeEnd", oObject);
}

var bPrivacyPolicyOpen = false;

function fnShowPrivacyPolicy()
{
    if (!bPrivacyPolicyOpen)
    {
        bPrivacyPolicyOpen = true;
        fnOpenMDIWindow(0, 0, "pages/privacy.html", "agreement.png", "Privacy Policy", true, true, false, '', 'fnClosePrivacyPolicy();', null);
    }
}

function fnClosePrivacyPolicy()
{
    bPrivacyPolicyOpen = false;
}


var bMsgBoxOpen = false;
var szMsgBoxOnOk = '';
var szMsgBox1 = '';
var szMsgBox2 = '';

function fnSaasMessageBox(message, title, MsgOptions, callbackid, idscr)
{
    try
    {
        if (!bMsgBoxOpen)
        {
            bMsgBoxOpen = true;
            var prefix = '';

            for (var wdno = 0; wdno < iMDIWindowsOpen; wdno++)
            {
                var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno]);

                if (oWindow.children[0].contentWindow.document.body.children[1].getAttribute("_screenindex") == idscr)
                    prefix = 'document.getElementById(iMDIOpenWindowsArray[' + wdno + ']).children[0].contentWindow.';
            }

            message = decodeURIComponent(message);
            title = decodeURIComponent(title);
            fnOpenFrame((maintop.clientWidth - 500) / 2, topbarsize + (maintop.clientHeight - 200) / 2, 500, 200, 'msgwindow', false, true, false, false, title, '', szMsgBox2, null, '');
            msgwindow.style.zIndex = 999;

            if (message.indexOf('<br>') > 0)
                fnCreateLabel(msgwindow, 'lblmsg', 70, 70, 410, 60, message, 'center');
            else
                fnCreateLabel(msgwindow, 'lblmsg', 70, 90, 410, 40, message, 'center');

            if (MsgOptions.indexOf("STOP") != -1 || MsgOptions.indexOf("EXCLAMATION") != -1 || MsgOptions.indexOf("INFORMATION") != -1)
                fnCreateLabel(msgwindow, 'lblicon', 10, 70, 64, 64, '<img src="images/icons64/alert.png" />');
            else if (MsgOptions.indexOf("QUESTION") != -1)
                fnCreateLabel(msgwindow, 'lblicon', 10, 70, 64, 64, '<img src="images/icons64/help---blue.png" />');

            if (MsgOptions.indexOf("OKCANCEL") != -1)
            {
                fnCreateButton(msgwindow, 'btok', 135, 150, 100, 25, szOk, fnCloseMsgBox1);
                fnCreateButton(msgwindow, 'btcancel', 335, 150, 100, 25, szCancel, fnCloseMsgBox2);
                szMsgBox1 = prefix + "SrvCommCallback(" + callbackid + ", 1);";
                szMsgBox2 = prefix + "SrvCommCallback(" + callbackid + ", 2);";
            }
            else
            {
                fnCreateButton(msgwindow, 'btok', 135, 150, 100, 25, szJa, fnCloseMsgBox1);
                fnCreateButton(msgwindow, 'btcancel', 335, 150, 100, 25, szNee, fnCloseMsgBox2);
                szMsgBox1 = prefix + "SrvCommCallback(" + callbackid + ", 6);";
                szMsgBox2 = prefix + "SrvCommCallback(" + callbackid + ", 7);";
            }
        }
    }
    catch (e) 
    {
        alert(e);
    }
}

function fnMsgBox(title, message, onok)
{
    if (typeof onok != 'undefined')
        showQinoMsgBox(message, title, "EXCLAMATION OK CANCEL", "", "", onok, "");
    else
        showQinoMsgBox(message, title, "EXCLAMATION CLOSE", "", "", "", "");
}

function fnCloseMsgBox1()
{
    bMsgBoxOpen = false;
    fnCloseFrame(msgwindow, false);
    eval(szMsgBox1);
}

function fnCloseMsgBox2()
{
    bMsgBoxOpen = false;
    fnCloseFrame(msgwindow, false);
    eval(szMsgBox2);
}

function fnCloseMsgBoxOK()
{
    bMsgBoxOpen = false;
    fnCloseFrame(msgwindow, false);
    eval(szMsgBoxOnOk);
}

function fnCloseMsgBox()
{
    bMsgBoxOpen = false;
    fnCloseFrame(msgwindow, false);
}

function fnOpenWaitWindow()
{
    showQinoBusy();
}

function fnCloseWaitWindow()
{
    hideQinoBusy();
}

var iMDIWindowsOpen = 0;
var idNextMDIWindow = 0;
var iMDIOpenWindowsArray = new Array();
var szMDIOpenWindowsArrayCloseFunc = new Array();
var szMDIOpenWindowsArrayURL = new Array();
var szMDIOpenWindowsArrayID = new Array();
var bMDIOpenWindowsArraySize = new Array();
var iMDILastWindowId = 0;
var szMDINextIcon = '';
var szMDIFuncOnOpen = '';
var szMDILastTitle = '';


function fnOpenMDIWindow(w, h, url, icon, title, bCloseBox, bSize, bRefresh, szFuncOnOpen, funcOnClose, fnHelp)
{
    var x;
    var y;
    var id;
    var wdno = iMDIWindowsOpen;

    bSize = true;

    iMDILastWindowId++;
    id = 'mdiwd' + iMDILastWindowId;
    iMDIOpenWindowsArray[wdno] = id;
    szMDIOpenWindowsArrayID[wdno] = idNextMDIWindow;
    szMDIOpenWindowsArrayCloseFunc[wdno] = funcOnClose;
    szMDIOpenWindowsArrayURL[wdno] = url;
    bMDIOpenWindowsArraySize[wdno] = bSize;
    iMDIWindowsOpen++;

    szMDINextIcon = icon;
    szMDIFuncOnOpen = szFuncOnOpen;
    szMDILastTitle = title;

    if (bSize)
    {
        w = maintop.clientWidth;
        h = maintop.clientHeight;
    }
    else
    {
        if (w > (maintop.clientWidth))
            w = maintop.clientWidth;

        if (h > (maintop.clientHeight))
            h = maintop.clientHeight;
    }

    x = (maintop.clientWidth - w) / 2;
    y = topbarsize + (maintop.clientHeight - h) / 2;

    fnOpenFrame((iMDIWindowsOpen - 1) * 150, 30, 150, 30, id + 'titlewindow', false, false, false, false, '', '', null, null, 'brainumwindowicon');
    fnOpenFrame(x, y, w, h, id, false, true, bSize, bRefresh, title, 'fnMDICompleteWindow(' + wdno + ');', fnMDICloseWindow, fnHelp, '');
}

function fnMDIHelp()
{
    alert('later...');
}

function fnMDICompleteWindow(wdno)
{
    var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno]);
    var oTitleWindow = document.getElementById(iMDIOpenWindowsArray[wdno] + 'titlewindow');
    var oObject = document.createElement("iframe");

    oObject.onreadystatechange = fnCheckComplete;
    fnOpenWaitWindow();

    oObject.frameBorder = "no";
    oObject.style.position = "absolute";
    oObject.style.top = "24px";
    oObject.style.left = "0px";
    oObject.style.width = (oWindow.clientWidth - 2) + "px";
    oObject.style.height = (oWindow.clientHeight - (bMDIOpenWindowsArraySize[wdno] ? 32 : 26)) + "px";
    oObject.onactivate = fnMDIMouseDown;
    oWindow.insertAdjacentElement("beforeEnd", oObject);

    var oIconText = document.createElement("BUTTON");

    oIconText.innerHTML = szMDILastTitle;
    oIconText.title = szMDILastTitle;
    oIconText.style.fontSize = "10px";
    oIconText.style.position = "absolute";
    oIconText.style.top = "0px";
    oIconText.style.left = "0px";
    oIconText.style.height = "30px";
    oIconText.style.width = "150px";
    oIconText.className = 'iconbaractive';
    oIconText.onclick = fnRestoreWindow;

    oTitleWindow.insertAdjacentElement("beforeEnd", oIconText);
    fnMDIMouseDown(oWindow);
    oObject.src = szMDIOpenWindowsArrayURL[wdno];

    if (szMDIFuncOnOpen != "")
        eval(szMDIFuncOnOpen);

    setTimeout('fnCloseWaitWindow();', 200);
}

function fnMDIMouseDown(obj)
{
    var id;
    var max = 0;

    if (typeof obj == 'undefined')
    {
        var target = event.target || event.srcElement;
        id = event.target.parentElement.id;
    }
    else
    {
        id = obj.id;
    }

    var ok = false;

    for (var i = 0; i < iMDIWindowsOpen; i++)
    {
        var oWindow = document.getElementById(iMDIOpenWindowsArray[i]);

        if (id == oWindow.id)
            ok = true;
    }

    if (!ok)
        return;

    for (var i = 0; i < iMDIWindowsOpen; i++)
    {
        var oWindow = document.getElementById(iMDIOpenWindowsArray[i]);
        var oTitleWindow = document.getElementById(iMDIOpenWindowsArray[i] + 'titlewindow');

        if (id == oWindow.id)
            oTitleWindow.children[0].className = 'iconbaractive';
        else
            oTitleWindow.children[0].className = 'iconbar';

        oWindow.style.zIndex = 1;
    }

    obj = document.getElementById(id);

    if (obj)
        obj.style.zIndex = 2;
}

function fnRestoreWindow()
{
    var target = this;
    var wdno = -1;

    for (var i = 0; i < iMDIWindowsOpen; i++)
    {
        if (target.parentElement.id == iMDIOpenWindowsArray[i] + 'titlewindow')
        {
            wdno = i;
            break;
        }
    }

    if (wdno >= 0)
    {
        var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno]);
        oWindow.style.display = '';
        fnMDIMouseDown(oWindow);
    }
}

function fnMDICloseWindow()
{
    var target = this;
    var wdno = -1;

    for (var i = 0; i < iMDIWindowsOpen; i++)
    {
        if (target.parentElement.parentElement.id == iMDIOpenWindowsArray[i])
        {
            wdno = i;
            break;
        }
    }

    if (wdno >= 0)
    {
        if (szMDIOpenWindowsArrayCloseFunc[wdno] != "")
            eval(szMDIOpenWindowsArrayCloseFunc[wdno]);

        fnMDICloseWindowGo(wdno);
    }
}

function fnMDICloseWindowGo(wdno)
{
    var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno]);
    var oTitleWindow = document.getElementById(iMDIOpenWindowsArray[wdno] + 'titlewindow');

    for (var i = wdno + 1; i < iMDIWindowsOpen; i++)
    {
        iMDIOpenWindowsArray[i - 1] = iMDIOpenWindowsArray[i];
        szMDIOpenWindowsArrayCloseFunc[i - 1] = szMDIOpenWindowsArrayCloseFunc[i];
        szMDIOpenWindowsArrayURL[i - 1] = szMDIOpenWindowsArrayURL[i];
        bMDIOpenWindowsArraySize[i - 1] = bMDIOpenWindowsArraySize[i];
    }

    iMDIWindowsOpen--;

    fnCloseFrame(oWindow, false);
    fnCloseFrame(oTitleWindow, false);

    var x = 0;

    for (var i = 0; i < iMDIWindowsOpen; i++)
    {
        var oTitleWindow = document.getElementById(iMDIOpenWindowsArray[i] + 'titlewindow');
        oTitleWindow.style.left = x + "px";
        x += 150;
    }

    if (iMDIWindowsOpen)
        fnMDIMouseDown(document.getElementById(iMDIOpenWindowsArray[iMDIWindowsOpen - 1]));
}

function ActivateScreen(wdno)
{
    if (wdno >= 0 && wdno < iMDIWindowsOpen)
    {
        var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno]);
        oWindow.style.display = '';
        fnMDIMouseDown(oWindow);
    }
}

function CloseScreen(id, screenid, szCtrl)
{
    for (var wdno = 0; wdno < iMDIWindowsOpen; wdno++)
    {
        var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno]);
        if (oWindow.children[0].contentWindow.document.body.children[1].getAttribute("_screenindex") == id)
        {
            SrvCommNaviDelete(id);
            fnMDICloseWindowGo(wdno);
        }
        else if (oWindow.children[0].contentWindow.document.body.children[1].getAttribute("_screenindex") == screenid)
        {
            if (wdno)
            {
                try
                {
                    setTimeout("try {var oWindow = document.getElementById(iMDIOpenWindowsArray[" + wdno + "]); oWindow.children[0].contentWindow.SrvComm(\"" + szCtrl + "\", \"OnChange\");} catch(e) {}", 100);
                } 
                catch (e) { }
            }
        }
    }
}

function fnCloseSystemScreen(wdno)
{
    try
    {
        var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno]);
        SrvCommNaviDelete(oWindow.children[0].contentWindow.document.body.children[1].getAttribute("_screenindex"));

        if (wdno)
        {
            try
            {
                var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno - 1]);
                oWindow.children[0].contentWindow.SrvCommScreen(oWindow.children[0].contentWindow.document.body.children[1].getAttribute("_screenindex"), 'OnActivate2');
            } 
            catch (e) { }
        }
    } 
    catch (e) { }
}

function ShowHelpScreen(id)
{
    clientopenscreen('E20767FC-AF76-49DA-B214-39821E25F352', id);
}

function clientopeneditscreen(id, tp)
{
    var html = '/system/server/screen/screen.define.asp?id=' + id + '&editmode=1&tp=' + tp + '&nocache=' + (new Date().getTime());
    OpenMDIScreen('', html, id);
}

function openhtmlscreen(title, url)
{
    var html = '/system/server/screen/screen.url.asp?url=' + encodeURIComponent(url) + '&title=' + title + '&nocache=' + (new Date().getTime());
    OpenMDIScreen(title, html);
}

function clientopenscreen(id, recordid, refscreen, refctrl, tablename, refid, readonly)
{
    if (typeof readonly == 'undefined')
        readonly = '0';

    var html = '/system/server/screen/screen.asp?id=' + id + '&recordid=' + recordid;

    if (typeof refscreen != 'undefined')
        html += '&refscreen=' + refscreen + '&refctrl=' + refctrl + '&tablename=' + tablename + '&refid=' + refid;

    html += "&readonly=" + readonly;
    html += "&scrwidth=" + (maintop.clientWidth);
    html += "&scrheight=" + (maintop.clientHeight - 34);
    html += "&nocache=" + (new Date().getTime());

    OpenMDIScreen('', html, id);
}

function dummyopenscreen()
{
    var html = '/system/server/screen/emptytree.html';
    OpenMDIScreen('Dummy', html);

    treethere = true;
}


function clientopenscreenname(name, recordid, refscreen, refctrl, tablename, refid)
{
    var html = '/system/server/screen/screen.asp?name=' + name + '&recordid=' + recordid;

    if (typeof refscreen != 'undefined')
        html += '&refscreen=' + refscreen + '&refctrl=' + refctrl + '&tablename=' + tablename + '&refid=' + refid;

    html += "&scrwidth=" + (maintop.clientWidth);
    html += "&scrheight=" + (maintop.clientHeight - 34);
    html += "&nocache=" + (new Date().getTime());

    OpenMDIScreen('', html, '', name);
}

function clientopenscreennameinittxt(name, recordid, inittxt)
{
    var html = '/system/server/screen/screen.asp?name=' + name + '&recordid=' + recordid;
    html += '&inittxt=' + encodeURIComponent(inittxt);
    html += "&scrwidth=" + (maintop.clientWidth);
    html += "&scrheight=" + (maintop.clientHeight - 34);
    html += "&nocache=" + (new Date().getTime());

    OpenMDIScreen('', html, '', name);
}

function clientopenscreenidstatus(id, status, exid)
{
    var html = '/system/server/screen/screen.asp?id=' + id;
    html += '&status=' + encodeURIComponent(status);

    if (typeof exid != 'undefined')
        html += '&exid=' + encodeURIComponent(exid);

    html += "&scrwidth=" + (maintop.clientWidth);
    html += "&scrheight=" + (maintop.clientHeight - 34);
    html += "&nocache=" + (new Date().getTime());

    OpenMDIScreen('', html, id);
}

function clientopenscreenvwd(id, vwd, recid, tablename)
{
    var html = '/system/server/screen/screen.asp?id=' + id;

    if (typeof recid != 'undefined')
        html += '&tablename=' + tablename + '&refid=' + recid;

    html += '&extvwd=' + vwd;
    html += "&scrwidth=" + (maintop.clientWidth);
    html += "&scrheight=" + (maintop.clientHeight - 34);
    html += "&nocache=" + (new Date().getTime());

    OpenMDIScreen('', html, id);
}

function clientopenscreennamevwd(name, vwd)
{
    var html = '/system/server/screen/screen.asp?name=' + name;

    html += '&extvwd=' + vwd;
    html += "&scrwidth=" + (maintop.clientWidth);
    html += "&scrheight=" + (maintop.clientHeight - 34);
    html += "&nocache=" + (new Date().getTime());

    OpenMDIScreen('', html, '', name);
}

function clientopenscreenCB(callbackid, id, recordid, refscreen, refctrl, tablename, refid)
{
    var html = '/system/server/screen/screen.asp?id=' + id + '&recordid=' + recordid;
    html += '&callbackid=' + callbackid;

    if (typeof refscreen != 'undefined')
        html += '&refscreen=' + refscreen + '&refctrl=' + refctrl + '&tablename=' + tablename + '&refid=' + refid;

    html += "&scrwidth=" + (maintop.clientWidth);
    html += "&scrheight=" + (maintop.clientHeight - 34);
    html += "&nocache=" + (new Date().getTime());

    OpenMDIScreen('', html, id);
}

function OpenMDIScreen(title, URL, id, name)
{
    title = title.replace('%20', ' ');

    if (typeof id != 'undefined')
    {
        var w = 0;
        var h = 0;
        var icon = "html.png";
        var title = "no title";
        var bCloseBox = true;
        var bSize = true;
        var bRefresh = false;

        idNextMDIWindow = id;

        if (typeof name == 'undefined')
            name = '';

        if (URL.indexOf('screen.define.asp') <= 0)
            eval(SrvCommGetScreenInfo(id, name));

        fnOpenMDIWindow(w, h, URL, icon, title, bCloseBox, bSize, bRefresh, '', 'fnCloseSystemScreen(' + iMDIWindowsOpen + ');');
    }
    else
    {
        fnOpenMDIWindow(0, 0, URL, "html.png", title, true, true, true, '', '', null);
    }
}

function SetTitle(id, title, ScreenNumberText)
{
    for (var wdno = 0; wdno < iMDIWindowsOpen; wdno++)
    {
        var oWindow = document.getElementById(iMDIOpenWindowsArray[wdno]);

        if (oWindow.children[0].contentWindow.document.body.children[1].getAttribute("_screenindex") == id)
        {
            var oTitleWindow = document.getElementById(iMDIOpenWindowsArray[wdno] + 'titlewindow');

            oTitleWindow.children[0].title = ScreenNumberText;      // dit is de tekst in de hoover popup
            oTitleWindow.children[0].innerText = title;             // dit is de tab titel met de schermnaam die komt uit die andere functie SetTitle ;-(
            
            if (szMDIOpenWindowsArrayURL[wdno].indexOf('screen.define.asp') > 0)
            {
                var html = '<span style="height: 18px; vertical-align: top;">' + title + '&nbsp;&nbsp;</span>';
                html += '&nbsp;<span style="height: 18px;" onclick="document.getElementById(iMDIOpenWindowsArray[' + wdno + ']).children[0].contentWindow.SavePos(true);" title="Save changes" ><img style="cursor: pointer; width: 18px; height: 18px;" src="/system/client/images/iconset/save_close.png" /></span>';
                html += '&nbsp;<span style="height: 18px;" onclick="clientopenscreen(\'' + szMDIOpenWindowsArrayID[wdno] + '\')" title="Test screen" ><img style="cursor: pointer; width: 18px; height: 18px;" src="/system/client/images/iconset/application_go.png" /></span>';
                html += '&nbsp;<span style="height: 18px;" onclick="document.getElementById(iMDIOpenWindowsArray[' + wdno + ']).children[0].contentWindow.fnReload();" title="Reload" ><img style="cursor: pointer; width: 18px; height: 18px;" src="/system/client/images/iconset/arrow_rotate_anticlockwise.png" /></span>';
                html += '&nbsp;<span style="height: 18px;" onclick="document.getElementById(iMDIOpenWindowsArray[' + wdno + ']).children[0].contentWindow.ScreenOptions();" title="Screen properties" ><img style="cursor: pointer; width: 18px; height: 18px;" src="/system/client/images/iconset/application_form_edit.png" /></span>';
                //                html += '&nbsp;<span style="height: 18px;" onclick="document.getElementById(iMDIOpenWindowsArray[' + wdno + ']).children[0].contentWindow.EditCode();" title="Edit code" ><img style="cursor: pointer; width: 18px; height: 18px;" src="/system/client/images/iconset/application_osx_terminal.png" /></span>';

                oWindow.children[1].children[0].innerHTML = html;
            }
            else
            {
                oWindow.children[1].children[0].innerText = title;
            }
            break;
        }
    }
}

function fnCheckComplete()
{
    if (this.readyState == "complete")
        fnCloseWaitWindow();
}

function IOx(Url)
{
    var X = new xmlhttp();
    X.open('GET', Url, false);
    X.setRequestHeader('Content-Type', 'text/xml');
    try { X.responseType = 'msxml-document'; } catch (e) { }
    X.send('');
    return X.responseXML;
}

var bmenuopen = false;
var bmenuopenid = '';

function CloseMainMenu()
{
    if (bmenuopen)
        fnCloseFrame(mainmenuwindow);

    bmenuopen = false;
    bmenuopenid = '';
}

function menu_handling(eventobject, xmlid)
{
    var target = this;

    try
    {
        var oElement = null;
        oElement = eventobject.target;
        var szType = eventobject.type;

        if (bmenuopen && szType == "click" && bmenuopenid == xmlid)
        {
            fnCloseFrame(mainmenuwindow);
            bmenuopen = false;
            bmenuopenid = '';
            return;
        }

        if (szType == "mouseover")
        {
            oElement.style.cssText = "background: #34495a";
        }

        if (szType == "mouseout")
        {
            oElement.style.cssText = "background: url(/saas/images/box-mid.png) repeat-x;";
        }

        if (szType == "click")
        {
            var szType = oElement.getAttribute("type");

            if (szType == "func")
            {
                CloseMainMenu();
                SrvCommMenu(xmlid, "OnMenu");
            }
            else
            {
                OpenSubMenu(xmlid, oElement.offsetLeft);
            }
            return;
        }
    }
    catch (e) { alert(e) }
}

function submenu_handling(eventobject, xmlid)
{
    var target = this;

    try
    {
        var oElement = null;
        oElement = eventobject.target;

        while (oElement.tagName != "TD")
        {
            oElement = oElement.parentElement;
        }

        if (oElement.getAttribute("type") == "sep")
            return;

        var szType = eventobject.type;

        if (szType == "mouseover")
        {
            oElement.style.background = "#34495a";
            oElement.style.color = "white";
        }

        if (szType == "mouseout")
        {
            oElement.style.background = "";
            oElement.style.color = "#212F3A";
        }

        if (szType == "click")
        {
            var szType = oElement.getAttribute("type");
            if (szType == "func")
            {
                SrvCommMenu(xmlid, "OnMenu");
                CloseMainMenu();
            }
            else
            {
                OpenSubMenu(xmlid);
            }
            return;
        }
    }
    catch (e) 
    { 
        alert(e.description) 
    }
}

function OpenSubMenu(menuid, leftpos)
{
    CloseMainMenu();

    var szBody = "<table z-index: 1999; cellpadding=0 cellspacing=0 style='width: 900px; background: white; font: 8pt Microsoft Sans Serif; cursor: default; table-layout: fixed' class=sub1><TBODY>";

    //let us fill the submenu:
    var oMenu = menuxml.selectSingleNode("/mainmenu");
    var oMenuItems = oMenu.getElementsByTagName("menu");
    var nLen = oMenuItems.length;
    var nSubLen = 0;
    var oSubItems = null;
    var bNoSub = false;
    var bFirst = true;
    var overFlow = false;
    var countrow = 3;
    var wm = 900;
    var height = 7;
    var title = 'Menu';
    var menuvlgid = 0;

    for (var nItem = 0; nItem < nLen; nItem++)
    {
        var oItem = oMenuItems[nItem];

        if (oItem.getAttribute("id") == menuid)
        { 
            //this is the one:
            title = oItem.getAttribute("name");
            oSubItems = oItem.selectNodes("menu1");
            nSubLen = oSubItems.length;
            var szType = oItem.getAttribute("type")

            if (nSubLen)
            {
                for (var nSubItem = 0; nSubItem < nSubLen; nSubItem++)
                {
                    var oSubItems_1 = oSubItems[nSubItem].selectNodes("menu2");

                    if (!oSubItems_1.length && oSubItems[nSubItem].getAttribute("type") != "sep")
                        bNoSub = true;
                }

                if (bNoSub)
                {
                    height += 25;

                    if (bFirst)
                        szBody += "<tr style='width: " + wm + "px; height: 25px; background: #e5e7e6;'><td colspan=" + countrow + " style='border-bottom: solid 1px #cad6e6'>";
                    else
                        szBody += "<tr style='width: " + wm + "px; height: 25px; background: #e5e7e6;'><td colspan=" + countrow + " style='border-top: solid 1px #cad6e6; border-bottom: solid 1px #cad6e6'>";

                    menuvlgid++;
                    szBody += "<div style='font: bold 12px Microsoft Sans Serif; color: #1e2f39; margin-left: 15px; margin-top: 4px;' onclick='hideshowsubmenu(" + menuvlgid + ")'>";
                    szBody += oItem.getAttribute("name");
                    szBody += "</div>";
                    szBody += "</td><tr><td style='height:7px;'></td><tr>";
                    height += 7;
                    bFirst = false;
                    var menuvlgid1 = 0;
                    var col = 0;

                    for (var nSubItem = 0; nSubItem < nSubLen; nSubItem++)
                    {
                        var oSubItems_1 = oSubItems[nSubItem].selectNodes("menu2");

                        if (!oSubItems_1.length && oSubItems[nSubItem].getAttribute("type") != "sep")
                        {
                            var szID = oSubItems[nSubItem].getAttribute("id");
                            if (!col)
                            {
                                height += 25;
                                szBody += "<tr id='menurow" + menuvlgid + "_" + menuvlgid1 + "' style='width: " + wm + "px; height: 25px;'>";
                                menuvlgid1++;
                            }

                            szBody += "<td type='" + oSubItems[nSubItem].getAttribute("type") + "' style='padding-top: 3px;width: 300px; height: 22px;color:#212F3A;'";
                            szBody += " onmouseover=\"submenu_handling(event, '" + szID + "')\"";
                            szBody += " onmouseout=\"submenu_handling(event, '" + szID + "');\"";
                            szBody += " onclick=\"submenu_handling(event, '" + szID + "');\"";
                            szBody += "><span style='font:12px Microsoft Sans Serif; overflow: hidden; width: 300px; height: 22px; margin-left: 15px; '>";
                            szBody += "<span style='background:url(/images/menu.png) no-repeat center'>&nbsp;</span>&nbsp;&nbsp;<nobr>"
                            szBody += oSubItems[nSubItem].getAttribute("name");
                            szBody += "</nobr></span></td>";

                            if (col == countrow - 1)
                                szBody += "</tr>";

                            col++;

                            if (col == countrow) 
                                col = 0;
                        }
                    }

                    if (col == 1 && countrow == 3)
                        szBody += "<td></td><td></td></tr>";

                    if (col == 2 || (col && countrow == 2))
                        szBody += "<td></td></tr>";

                    height += 7;
                    szBody += "<tr><td style='height:7px;'></td></tr>";
                }

                for (var nSubItem = 0; nSubItem < nSubLen; nSubItem++)
                {
                    var oSubItems_1 = oSubItems[nSubItem].selectNodes("menu2");

                    if (oSubItems_1.length)
                    {
                        height += 25;

                        if (bFirst)
                            szBody += "<tr style='width: " + wm + "px; height: 25px; background: #e5e7e6;'><td colspan=" + countrow + " style='border-bottom: solid 1px #cad6e6'>";
                        else
                            szBody += "<tr style='width: " + wm + "px; height: 25px; background: #e5e7e6;'><td colspan=" + countrow + " style='border-top: solid 1px #cad6e6; border-bottom: solid 1px #cad6e6'>";

                        menuvlgid++;
                        szBody += "<div style='font: bold 12px Microsoft Sans Serif; color: #1e2f39; margin-left: 15px; padding-top: 4px;' onclick='hideshowsubmenu(" + menuvlgid + ")'>";
                        szBody += oSubItems[nSubItem].getAttribute("name");
                        szBody += "</div>";
                        szBody += "</td><tr><td style='height:7px;'></td></tr><tr>";
                        height += 7;
                        bFirst = false;

                        var menuvlgid1 = 0;
                        var col = 0;
                        var nSubLen1 = oSubItems_1.length;

                        for (var nSubItem1 = 0; nSubItem1 < nSubLen1; nSubItem1++)
                        {
                            if (oSubItems_1[nSubItem1].getAttribute("type") != "sep")
                            {
                                var szID = oSubItems_1[nSubItem1].getAttribute("id");

                                if (!col)
                                {
                                    height += 25;
                                    szBody += "<tr id='menurow" + menuvlgid + "_" + menuvlgid1 + "' style='width: " + wm + "px; height: 25px;'>";
                                    menuvlgid1++;
                                }

                                szBody += "<td type='" + oSubItems_1[nSubItem1].getAttribute("type") + "' style='padding-top: 3px; width: 300px; height: 22px;color:#212F3A;'";
                                szBody += " onmouseover=\"submenu_handling(event, '" + szID + "')\"";
                                szBody += " onmouseout=\"submenu_handling(event, '" + szID + "');\"";
                                szBody += " onclick=\"submenu_handling(event, '" + szID + "');\"";
                                szBody += "><span style='font: 12px Microsoft Sans Serif; height: 22px; margin-left: 15px;'>";
                                szBody += "<span style='background:url(/images/menu.png) no-repeat center'>&nbsp;</span>&nbsp;&nbsp;"
                                szBody += oSubItems_1[nSubItem1].getAttribute("name");
                                szBody += "</span></td>";

                                if (col == countrow - 1)
                                    szBody += "</tr>";

                                col++;

                                if (col == countrow)
                                    col = 0;
                            }
                        }

                        if (col == 1 && countrow == 3)
                            szBody += "<td></td><td></td></tr>";

                        if (col == 2 || (col && countrow == 2))
                            szBody += "<td></td></tr>";

                        szBody += "<tr><td style='height:7px;'></td></tr>";
                        height += 7;
                    }
                }

            }
        }
    }
    //end of filling
    
    szBody += "</TBODY></TABLE>";

    if (leftpos + 900 > maintop.clientWidth)
        leftpos -= leftpos + 900 - maintop.clientWidth;

    if (leftpos < 0) 
        leftpos = 0;

    fnOpenFrame(leftpos, 30, 900, height, 'mainmenuwindow', false, false, false, false, '', '', CloseMainMenu, null, 'brainumwindow');

    var oObject = document.createElement("div");

    oObject.style.position = "absolute";
    oObject.style.top = "0px";
    oObject.style.left = "0px";
    oObject.style.width = "900px";
    oObject.style.height = height + "px";
    oObject.innerHTML = szBody;

    mainmenuwindow.insertAdjacentElement("beforeEnd", oObject);
    mainmenuwindow.style.zIndex = 1999;
    bmenuopen = true;
    bmenuopenid = menuid;
}

function ShowTopMenu()
{
    try
    {
        if (!bLoggedOn)
            return false;

        if (bmenuopen)
        {
            fnCloseFrame(mainmenuwindow);
            bmenuopen = false;
            bmenuopenid = '';
            return;
        }

        var tbTopbar = document.getElementById("tbTopbar");
        var oMenu = menuxml.selectSingleNode("/mainmenu");
        var oMenuItems = oMenu.getElementsByTagName("menu");
        var nLen = oMenuItems.length;
        var html = '<table cellpadding="0" cellspacing="0"><tr>';
        var nSubLen = 0;

        for (var nItem = 0; nItem < nLen; nItem++)
        {
            var oItem = oMenuItems[nItem];
            var szID = oItem.getAttribute("id");

            html += "<td style='padding: 0px; width:1px'><img src='/saas/images/box-left.png' /></td><td>";
            html += "<td style='padding: 0px'>";
            html += "<div class='menubox' onmouseover='menu_handling(event, \"" + szID + "\")' onmouseout='menu_handling(event, \"" + szID + "\")' onclick='menu_handling(event, \"" + szID + "\")' >";
            html += oItem.getAttribute("name");
            html += "</div></td><td style='padding: 0px; width:1px'><img src='/saas/images/box-right.png' /></td>";
        }

        html += "<td style='padding: 0px; width:1px'><img src='/saas/images/box-left.png' /></td></tr></table>";
        tbTopbar.cells[0].innerHTML = html;
    } 
    catch (e) 
    {
        alert(e.description);
    }
}

function hideshowsubmenu(id)
{
    var id1 = 0;
    var obj = document.getElementById("menurow" + id + "_" + id1);

    while (obj != null)
    {
        if (obj.style.display == 'none')
            obj.style.display = '';
        else
            obj.style.display = 'none';

        id1++;
        obj = document.getElementById("menurow" + id + "_" + id1);
    }
}

