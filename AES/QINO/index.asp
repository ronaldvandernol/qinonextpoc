<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@CODEPAGE=65001%>
<%
	Response.CharSet = "UTF-8";
%>
<%var loginpage = 1; %>
<!-- #include virtual="/system/server/database/databaseclass.asp" -->
<!-- #include virtual="/system/server/javascript/functions.asp" -->
<!-- #include virtual="/system/server/application/applicationclass.asp" -->
<%
	var app = new App();
	app.init();
%>
<head>
    <title>Brainum :: <%Response.Write(app.title); %></title>
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="/system/client/css/standard.css" />
    <link rel="stylesheet" type="text/css" href="css/standard.css" />
</head>
<body>
<script language=javascript>
    var szLogonTitle = <%TDQ("Inloggen")%>;
    var szTabletMode = <%TDQ("Tablet/Mobile")%>;
    var szCloseScreen = <%TDQ("Sluit venster")%>;
    var szMaximaliseer = <%TDQ("Maximaliseer")%>;
    var szMinimaliseer = <%TDQ("Minimaliseer")%>;
    var szPasgroottevensteraan = <%TDQ("Pas grootte venster aan")%>;
    var szVerversen = <%TDQ("Verversen")%>;
    var szHelp = <%TDQ("Help")%>;
    var szInlognaam = <%TDQ('Inlognaam')%>;
    var szInlognaamonthouden = <%TDQ('Inlognaam onthouden')%>;
    var szWachtwoord = <%TDQ('Wachtwoord')%>;
    var szInloggen = <%TDQ('Inloggen')%>;
    var szUmoeteeninlognaamingeven = <%TDQ('U moet een inlognaam ingeven.')%>;
    var szFoutmelding = <%TDQ('Foutmelding')%>;
    var szErisgeenantwoordvandeserverontvangenverversdepaginaenprobeeropnieuwaub = <%TDQ('Er is geen antwoord van de server ontvangen, ververs de pagina en probeer opnieuw aub.')%>;
    var szUheefteenongeldigwachtwoordofinlognaamingegeven = <%TDQ('U heeft een ongeldig wachtwoord of inlognaam ingegeven!')%>;
    var szDeserverkandedatabasenietbenaderenprobeerhetlaternogeensaub = <%TDQ('De server kan de database niet benaderen, probeer het later nog eens aub')%>;
    var szUwsessieisverlopenverversdepaginaenprobeeropnieuwaub = <%TDQ('Uw sessie is verlopen, ververs de pagina en probeer opnieuw aub')%>;
    var szEriseenongeldigvandeserverontvangenverversdepaginaenprobeeropnieuwaub = <%TDQ('Er is een ongeldig van de server ontvangen, ververs de pagina en probeer opnieuw aub')%>;
    var szJa = <%TDQ('Ja')%>;
    var szNee = <%TDQ('Nee')%>;
    var szOk = <%TDQ('Ok')%>;
    var szCancel = <%TDQ('Annuleer')%>;
    var topbarsize = 60;
    var szLogonCompany = <%TDQ("Bedrijf")%>;

function fnResetPW()
{
    var check = xmlhttp();
    check.open("post", "/system/server/user/mailuser.asp", false);
    check.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    check.send("username=" + encodeURIComponent(logonname.value));
    alert(<%TDQ('Een nieuw wachtwoord is verzonden (als uw gebruikersnaam bekend is)')%>);
}

function fnShowLoginError()
{
    <%
    cpbGOTORECORD("Instellingen", 1);
    if(app.Instellingen_FormPasswordReset) {%>
	    fnMsgBox(<%TDQ("Foutieve ingave")%>, <%TDQ("Ongeldige inlognaam of wachtwoord. Wilt u een nieuw wachtwoord ontvangen?")%>, "fnResetPW()");
    <%} else { %>
        fnMsgBox(szInloggen, szUheefteenongeldigwachtwoordofinlognaamingegeven);
    <%} %>
}
</script>
<script type="text/javascript" src="js/standard.js"></script>
<script type="text/javascript" src="/system/client/javascript/cookie.js"></script>
<script type="text/javascript" src="/system/client/javascript/srvcomm.js"></script>
<script type="text/javascript" src="/system/client/javascript/browserinfo.js"></script>
<script type="text/javascript" src="/system/client/javascript/functions.js"></script>
<script type="text/javascript" src="/system/client/javascript/aes.js"></script>
<!-- #include virtual="/system/client/screen/qinoshowdialogs.asp" -->
<div id="maintemp" class="default" style="width: 1px; height: 100%; position: absolute">
</div>
<div id="main" class="default" style="width: 100%; height: 100%;">
<div id="mainbot" class="bottom" style="width: 100%; height: 60px; overflow: hidden">
<table cellpadding="0" cellspacing="0" width="100%">
  <tr id="tbTopbar">
    <td style="text-align: left; height: 26px" class="topboxnopad">&nbsp;</td>
    <td style="text-align: right; height: 26px; overflow: hidden">
      <div style="text-align: right; height: 26px; overflow: hidden" id="tdlogon" class="topbox">
        <img src="/system/client/images/iconset/lock.png" style="height: 24px; margin: 0px; display: inline;" ><p style="display: inline; margin: 0px; margin-left: 5px; padding: 0px; vertical-align: 50%;"><%T("Niet aangemeld")%></p>
      </div>
    </td>
  </tr>
</table>
</div>
<div id="maintop" class="default" style="width: 100%; height: 400px">
</div>
</div>

<script type="text/javascript" language="javascript">

    function fnGetCompanies()
    {
        return <%
        var ReturnCompanies = "";
        var db = new Database(app, "APP");
        if (db.connect())
        {
            var rsCompany = db.querystartfast("SELECT [Company].[Id], [Company].[Name] FROM [Company] ORDER BY [Company].[Name]");
            while (!rsCompany.EOF)
            {
                ReturnCompanies += rsCompany("Id").value + rsCompany("Name").value + ";"; 
                rsCompany.MoveNext();
            }
            rsCompany.close();
        }
        Response.Write("'" + ReturnCompanies + "';");
        %>
    }

    function fnGetCurrentCompanyId()
    {
        return <%
        var ReturnCurrentCompany = "";
        var db = new Database(app, "APP");
        if (db.connect())
        {
            var rsCompany = db.querystartfast("SELECT TOP 1 [Settings].[CompanyId] FROM [Settings]");
            if (!rsCompany.EOF)
            {
                ReturnCurrentCompany = rsCompany("CompanyId").value; 
            }
            rsCompany.close();
        }
        Response.Write("'" + ReturnCurrentCompany + "';");
        %>
    }

    var username = getUsername();
    if (!username)
        window.location.replace('http://localhost:32150/#/login');

    setTimeout('fnResizeMainWindow()', 100);
    setTimeout('fnLogin("' + fnGetCompanies() + '","' + fnGetCurrentCompanyId() + '")', 110);
    setTimeout('SrvCommLogin("' + username + '","")', 120);
    setTimeout('fnShowLanguages()', 120);

  <%  
if (typeof Session("ActiveDirectoryName") != "undefined")
    {
        %>setTimeout('logonname.value = "<%=Session("ActiveDirectoryName")%>"; fnOnLogon()', 110);
        <%
    }
  %>
  var menuxml = new xmlhttp();
  menuxml.async = false;

  function fnShowLanguages()
   {
    var lu = new Cookie();
    var taal = lu.Get("brainumlang");
    if ("<%Response.Write(GetRequestVar("ok"))%>" == "undefined" && taal != null && taal != "null")
      {
	     location='index.asp?setLang=' + taal + '&ok=1';
	  }
  	<%
		CheckProgDatabaseConnection();

		var rs = db.querystartfast("select * from Language where (hide = 0 or hide is null) order by Language");
		if (!rs.EOF)
			 {
				%>fnCreateLabel(loginwindow, 'lbllanguage', 400, 180, 400, 20, 'Select your language:');<%
				}

		var x = 200;
		while (!rs.EOF)
			{
			 %>fnCreateLabel(loginwindow, 'lbllanguage', 400, <%Response.Write(x)%>, 400, 20, '<div style="cursor: hand" onclick="setlan(\'<%Response.Write(rs("Code").Value)%>\');"><img border="0" src="<%Response.Write(rs("Image").Value)%>">&nbsp;<%Response.Write(rs("Language").Value)%></div>');<%
			 rs.MoveNext();
			 x += 20;
			}
		%>
		}

  function getUrlParameter( name, url ) {
      if (!url) url = location.href;
      name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
      var regexS = "[\\?&]"+name+"=([^&#]*)";
      var regex = new RegExp( regexS );
      var results = regex.exec( url );
      return results == null ? null : results[1];
  }

  function getUsername() {
      var AESKey = getUrlParameter('aeskey');

      if(!AESKey){
          return null;
      }

      var decryptedAESKey = CryptoJS.AES.decrypt(AESKey,"a2e94370432f9ed0e2bc56be74c9120008eb7f30086df442ad5c87ce40da0ad3");
      var decryptedJsonString = decryptedAESKey.toString(CryptoJS.enc.Utf8);

      if(!decryptedJsonString){
          return null;
      }

      var obj = JSON.parse(decryptedJsonString);
      var jsonDateTime = new Date(obj.datetime);

      var maxValidateDateTime = new Date(jsonDateTime.getTime() + 30000);

      var currentTime = new Date();

      if (maxValidateDateTime < currentTime){
          return null;
      }
      return obj.username;
  }
</script>
</body>
</html>
