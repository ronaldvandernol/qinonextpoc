﻿<%var loginpage = 1; %>
<!-- #include virtual="/system/server/database/databaseclass.asp" -->
<!-- #include virtual="/system/server/javascript/functions.asp" -->
fnLogonResponse('<%
var app = Session("app");
if(typeof app == 'undefined')
{
		Response.Write("EXPIRED");
}
else
{
	db = new Database(app, "APP");		
	if (db.connect())
	{
		var rs;
		var username = GetRequestVar("username");
		var compid = GetRequestVar("compid");
		
		var nexturl = GetRequestVar("nexturl");

		username = username.replace(/'/g, '');
		compid = compid.replace(/'/g, '');
		nexturl = nexturl.replace(/'/g, '');

		var company = null;
		
		app.bSaas = cpbVAL(GetRequestVar("saas"));
		app.bIE = cpbVAL(GetRequestVar("ie"));
		app.tabletmode = cpbVAL(GetRequestVar("tablet"));

		if(username != "undefined" && username != "")
		{
            rs = db.querystart("select top 1 U.* from [User] U where U.Username = '" + username + "'");
		}
		if(!rs.EOF)
		{
			app.lastlogon = "";
			if(rs("LastLogonTime"))
			{
				var t = Date();
				try { t = Date.parse(rs("LastLogonTime") + "");
				var d = new Date(t);				
				app.lastlogon = d.toString(); } catch(e) {}
			}
			app.user = rs("Id").value;

		    if(company == null) // set app.company
            {
			    if(rs("CompanyId").value != null)
			        app.company = rs("CompanyId").value;
            }
            else
                app.company = company;

			app.szScheidingsteken = ";";
			if (rs("csvscheidingsteken").value != null)
				app.szScheidingsteken = rs("csvscheidingsteken").value;
			else if (Application("csvseperator"))
				app.szScheidingsteken = Application("csvseperator");

			app.userid = rs("UserId").value;
			app.username = rs("username").value;
			app.usermenu = rs("usermenu").value;
			app.admin = false;
			app.editselection = false;
			app.editdefaultselection = false;
			app.callbackid = 0;
			app.callbackidarray = new Array();
			app.callbacktype = new Array();
			app.callbackcode = new Array();
			app.callbackscreen = new Array();
			app.callbackprog = new Array();
			app.loggedin = 1;
            app.szOntwerp = fnGetTextTag('ScreenVar', 'Ontwerp');
            app.szNieuw = fnGetTextTag('ScreenVar', 'Nieuw');
            app.szWijzig = fnGetTextTag('ScreenVar', 'Wijzig');
            app.szBekijk = fnGetTextTag('ScreenVar', 'Bekijk');
			
			Response.Write("OK");
			
            var d = new Date();
            rs("LastLogonTime") = fmtdate(d);
            rs("FailedLoginCounter") = 0;
            rs("SessionCookieId").value =  Request.ServerVariables("HTTP_COOKIE");
            var sessioncookie = rs("SessionCookieId").value;
            rs("SessionCookieId").value = sessioncookie.substr( (sessioncookie.indexOf("ASPSESSIONID")+12), 33 );
            rs.Update();
            rs.close();
			
			app.settingsid = 1;
			rs = db.querystartfast("SELECT RecordID FROM Company WHERE (Id = '" + app.company + "')"); // retreive app.settingsid
			if(!rs.EOF)
		        app.settingsid = rs(0).value;
			rs.close();		

			rs = db.querystart("SELECT TOP 1 CompanyId FROM Settings"); // write last selected company to database.
			if(!rs.EOF)
            {
		        rs("CompanyId").value = app.company;
                rs.Update();
            }
			rs.close();		
    	
			rs = db.querystartfast("select usergroup.* from UsersPerUserGroup inner join usergroup on UsersPerUserGroup.usergroup = usergroup.id where (UsersPerUserGroup.[User] = '" + app.user + "')");
			while(!rs.EOF)
			{
				if(rs("admin").value)
					app.admin = true;
				if(rs("editselection").value)
					app.editselection = true;
				if(rs("EditDefaultSelection").value)
					app.editdefaultselection = true;
				rs.MoveNext();
			}
			rs.close();
			
			//reverse screenlocks			
			rs = db.querystart("select id from ScreenLocks where UserId = '" + app.user + "'");
			while(!rs.EOF)
            {
                
                rs.MoveNext();
            }
            rs.close();
            db.execute("delete from ScreenLocks where UserId = '" + app.user + "'");
            
			Main();
			
			try {IntegrateStart();} catch(e) {}
		}
		else	
        {
            rs.close();
			Response.Write("ERR");
        }
	}
	else
		Response.Write("DBERR");
}
	if(dbapp)
	    dbapp.disconnect();
	if(db)
	    db.disconnect();
%>');