﻿using Resources.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resources.LogicLayer
{
    public class ResourcesLogic
    {
        private readonly QINOResourcesServerEntities _context;
        public ResourcesLogic()
        {
            _context = new QINOResourcesServerEntities();
        }

        public bool DoesDatabaseExist()
        {
            return _context.Database.Exists();
        }

        public List<Order> GetOrders(List<string> terminalNames = null)
        {
            if (terminalNames == null)
                return _context.Orders.ToList();
            else
                return _context.Orders.Where(item => terminalNames.Contains(item.Terminal.Name)).ToList();
        }
    }
}
