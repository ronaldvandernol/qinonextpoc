﻿using Resources.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace AngularJSAuthentication.API.Controllers
{
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        //[Authorize(Roles = "GlobalPlanner")]
        [Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

            var terminalClaims = principal.Claims.Where(c => c.Type == "terminal").ToList();

            List<string> terminalnames = new List<string>();
            foreach (var lkj in terminalClaims)
                terminalnames.Add(lkj.Value);

            var sdf = principal.Claims.Any(item => item.Type == ClaimTypes.Role && item.Value == "GlobalPlanner");

            if (sdf)
                return Ok(Order.CreateOrders());
            else
                return Ok(Order.CreateOrders(terminalnames));
        }
    }


    #region Helpers

    public class Order
    {
        public int OrderID { get; set; }
        public string OrderType { get; set; }
        public string CustomerName { get; set; }
        public string ShipperCity { get; set; }
        public Boolean IsShipped { get; set; }
        public string TerminalName { get; set; }


        public static List<Order> CreateOrders(List<string> terminalNames = null)
        {
            Resources.LogicLayer.ResourcesLogic logic = new Resources.LogicLayer.ResourcesLogic();
            List<Order> OrderList = logic.GetOrders(terminalNames).Select(item => new Order()
            {
                OrderID = item.OrderId,
                OrderType = item.Type,
                TerminalName = item.Terminal.Name,
                CustomerName = "BP",
                IsShipped = true,
                ShipperCity = item.Product
            }).ToList();

            return OrderList;
        }
    }

    #endregion
}
