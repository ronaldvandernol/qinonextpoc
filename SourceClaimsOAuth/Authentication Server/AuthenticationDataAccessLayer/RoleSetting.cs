//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuthenticationDataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class RoleSetting
    {
        public int RoleId { get; set; }
        public int SettingId { get; set; }
        public string Value { get; set; }
    
        public virtual Role Role { get; set; }
        public virtual Setting Setting { get; set; }
    }
}
