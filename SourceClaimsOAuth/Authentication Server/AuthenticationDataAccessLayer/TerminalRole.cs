//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuthenticationDataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class TerminalRole
    {
        public int RoleIdTerminal { get; set; }
        public Nullable<int> ClientId { get; set; }
        public int TerminalId { get; set; }
        public Nullable<int> TextId { get; set; }
        public string Name { get; set; }
    
        public virtual Client Client { get; set; }
        public virtual Role Role { get; set; }
        public virtual Terminal Terminal { get; set; }
    }
}
