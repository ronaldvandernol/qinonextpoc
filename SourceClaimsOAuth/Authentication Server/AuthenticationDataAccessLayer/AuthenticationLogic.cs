﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticationDataAccessLayer
{
    public class AuthenticationLogic
    {
        private readonly QINOAuthenticationServerEntities _context;
        public AuthenticationLogic()
        {
            _context = new QINOAuthenticationServerEntities();
        }

        public bool DoesDatabaseExist()
        {
            return _context.Database.Exists();
        }

        public Person GetUser(string userName, string passWord)
        {
            return _context.People.FirstOrDefault(item => item.Name == userName && item.ClientLogin.Login.Email == passWord);
        }

        public List<GlobalRole> GetGlobalRoles(int loginID)
        {
            Login login = _context.Logins.First(item => item.LoginId == loginID);

            return login.GlobalRoles.ToList();
        }
    }
}