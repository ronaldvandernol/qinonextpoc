﻿'use strict';
app.controller('welcomeController', ['$scope', 'welcomeService', function($scope, welcomeService)
{

    $scope.orders = [];

    ordersService.getOrders().then(function(results)
    {

        $scope.orders = results.data;

    }, function(error)
    {
        //alert(error.data.message);
    });

}]);