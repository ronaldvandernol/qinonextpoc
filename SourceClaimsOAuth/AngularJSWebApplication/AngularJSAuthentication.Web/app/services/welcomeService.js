﻿'use strict';
app.factory('welcomeService', ['$http', 'ngAuthSettings', function($http, ngAuthSettings)
{

    //var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var serviceBase = 'http://localhost:47039/'; //TODO: hardcoded stuff for the resource server :(

    var welcomeServiceFactory = {};

    var _getOrders = function()
    {

        return $http.get(serviceBase + 'api/orders').then(function(results)
        {
            return results;
        });
    };

    welcomeServiceFactory.getOrders = _getOrders;

    return welcomeServiceFactory;

}]);